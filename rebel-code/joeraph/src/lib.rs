use std::{
    cmp::{Eq, PartialEq},
    collections::{HashMap, HashSet, VecDeque},
    convert::From,
    fmt::Debug,
    hash::{Hash, Hasher},
    io::Write,
    ops::Not,
};

use rand::prelude::*;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum EdgeType {
    Undirected,
    Directed,
}

#[derive(Clone, Debug)]
pub struct Node {
    pub id: usize,
    pub data: Option<String>,
}
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Bipartition {
    White,
    Black,
}

impl Not for Bipartition {
    type Output = Self;
    fn not(self) -> Self::Output {
        match self {
            Bipartition::Black => Bipartition::White,
            Bipartition::White => Bipartition::Black,
        }
    }
}

impl Node {
    pub fn plain(id: usize) -> Self {
        Node { id, data: None }
    }
}

impl From<usize> for Node {
    fn from(id: usize) -> Self {
        Node::plain(id)
    }
}

impl Hash for Node {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Node {}

#[derive(Debug, Clone)]
pub struct Edge {
    pub from: usize,
    pub to: usize,
    pub weights: Vec<f64>,
}

impl From<(usize, usize)> for Edge {
    fn from(e: (usize, usize)) -> Self {
        Edge::plain(e.0, e.1)
    }
}

impl Edge {
    pub fn plain(from: usize, to: usize) -> Self {
        let weights = vec![0.0];
        Edge { from, to, weights }
    }

    pub fn new(from: usize, to: usize, weights: Vec<f64>) -> Self {
        Edge { from, to, weights }
    }

    pub fn add_weight(&mut self, weight: f64) {
        self.weights.push(weight);
    }

    pub fn merge(&mut self, other: &mut Self) {
        self.weights.append(&mut other.weights);
    }

    pub fn count(&self) -> usize {
        self.weights.len()
    }

    pub fn weights(&self) -> impl Iterator<Item = &f64> {
        self.weights.iter()
    }

    pub fn size(&self) -> usize {
        self.weights.len()
    }

    pub fn id(&self) -> (usize, usize) {
        (self.from, self.to)
    }
}

fn sort_edge(e: (usize, usize)) -> (usize, usize) {
    let mut a = [e.0, e.1];
    a.sort_unstable();
    (a[0], a[1])
}

pub struct Graph {
    nodes: HashMap<usize, Option<String>>,
    // "children" in a directed graph
    neighbors: HashMap<usize, HashSet<usize>>,
    edges: HashMap<(usize, usize), Edge>,
    // to support removing edges when a node is removed
    parents: HashMap<usize, HashSet<usize>>,
    edge_type: EdgeType,
}

// basically taken from https://depth-first.com/articles/2020/01/06/a-minimal-graph-api/
impl Graph {
    // builders
    pub fn new(edge_type: EdgeType) -> Self {
        Graph {
            nodes: HashMap::new(),
            neighbors: HashMap::new(),
            edges: HashMap::new(),
            parents: HashMap::new(),
            edge_type,
        }
    }

    pub fn directed() -> Self {
        Graph::new(EdgeType::Directed)
    }

    pub fn undirected() -> Self {
        Graph::new(EdgeType::Undirected)
    }

    // output graphiz
    pub fn gdump(&self) -> String {
        let head = "strict graph {".to_string();
        let tail = "}".to_string();
        let nodes = {
            let mut nodes: Vec<String> = Vec::new();
            for node in self.nodes.keys() {
                nodes.push(format!("  n{}", node));
            }
            nodes.join("\n")
        };
        let edges = {
            let mut edges: Vec<String> = Vec::new();
            for edge in self.edges() {
                let (a, b) = edge.id();
                let label = edge.size();
                edges.push(format!("  n{} -- n{} [label={}]", a, b, label));
            }
            edges.join("\n")
        };
        [head, nodes, edges, tail].join("\n")
    }

    // TODO: store Nodes directly
    pub fn nodes(&self) -> impl Iterator<Item = usize> + '_ {
        self.nodes.keys().cloned()
    }

    pub fn get_data(&self, id: usize) -> Option<&String> {
        if let Some(data) = self.nodes.get(&id) {
            data.as_ref()
        } else {
            None
        }
    }

    // number of NODES
    pub fn order(&self) -> usize {
        self.nodes.len()
    }

    // how many outgoing edges from a node
    pub fn degree(&self, id: usize) -> Result<usize, String> {
        if let Ok(iter) = self.neighbors(id) {
            Ok(iter.count())
        } else {
            Err(format!("No node with ID '{}'", id))
        }
    }

    pub fn neighbors(&self, id: usize) -> Result<impl Iterator<Item = usize> + '_, String> {
        // check to see if this node is in the graph
        if !self.nodes.contains_key(&id) {
            return Err(format!("No node with ID '{}'", id));
        }

        let empty = HashSet::new();
        let mut children = self.neighbors.get(&id).unwrap_or(&empty).clone();
        if self.edge_type == EdgeType::Undirected {
            let parents = self.parents.get(&id).unwrap_or(&empty);
            children = children.union(parents).copied().collect();
        }

        Ok(children.into_iter())
    }

    // number of EDGES, *including parallel* ones if that's the edge type in the
    // graph.
    pub fn size(&self) -> usize {
        self.edges.values().map(|e| e.size()).sum()
    }

    pub fn edges(&self) -> impl Iterator<Item = &Edge> {
        self.edges.values()
    }

    pub fn edge(&self, edge: (usize, usize)) -> Result<&Edge, String> {
        let edge = if self.edge_type == EdgeType::Undirected {
            let mut edge = [edge.0, edge.1];
            edge.sort_unstable();
            (edge[0], edge[1])
        } else {
            edge
        };

        self.edges
            .get(&edge)
            .ok_or(format!("No edge from {} to {}", edge.0, edge.1))
    }

    pub fn add_node(&mut self, node: Node) -> Option<String> {
        self.nodes.insert(node.id, node.data).unwrap_or(None)
    }

    pub fn add_edge(&mut self, edge: Edge) -> bool {
        //
        // returns true if the edge was already present
        //
        let Edge { from, to, weights } = edge;
        let _ = self.nodes.entry(from).or_insert(None);
        let _ = self.nodes.entry(to).or_insert(None);
        let (from, to) = if self.edge_type == EdgeType::Undirected {
            sort_edge((from, to))
        } else {
            (from, to)
        };

        self.neighbors
            .entry(from)
            .or_insert_with(HashSet::new)
            .insert(to);
        self.parents
            .entry(to)
            .or_insert_with(HashSet::new)
            .insert(from);
        let mut edge = Edge::new(from, to, weights);
        if let Some(current) = self.edges.get_mut(&(from, to)) {
            current.merge(&mut edge);
            true
        } else {
            self.edges.insert((from, to), edge);
            false
        }
    }

    pub fn add_nodes(&mut self, nodes: &[Node]) -> usize {
        let mut updates = 0;
        for node in nodes.iter() {
            if self.add_node(node.clone()).is_some() {
                updates += 1;
            }
        }
        updates
    }

    // returns count of how many edges already existed
    pub fn add_edges(&mut self, edges: &[Edge]) -> usize {
        let mut updates = 0;
        for edge in edges.iter() {
            if self.add_edge(edge.clone()) {
                updates += 1;
            }
        }
        updates
    }

    pub fn edge_type(&self) -> EdgeType {
        self.edge_type
    }

    // methods that use the previous ones to implement fancier algorithms, like
    // searching and finding connected components
    fn bfs_bipartite(
        &self,
        start: &usize,
    ) -> Result<(impl Iterator<Item = usize> + '_, bool), String> {
        if !self.nodes.contains_key(start) {
            return Err(format!("No node with ID '{}'", start));
        }

        let mut nodes = Vec::with_capacity(self.order());
        let mut colors = HashMap::with_capacity(self.order());
        let mut discovered = HashSet::with_capacity(self.order());
        let mut is_bipartite = true;
        let mut q = VecDeque::new();

        q.push_back(*start);
        let _ = colors.insert(*start, Bipartition::Black);
        while !q.is_empty() {
            let node = q.pop_front().unwrap(); // if it were None, we'd not be
                                               // in the loop

            if !discovered.contains(&node) {
                nodes.push(node);
                discovered.insert(node);

                for neighbor in self.neighbors(node).unwrap() {
                    q.push_back(neighbor);

                    if is_bipartite {
                        // OK, now check bipartite stuff
                        let new_color = colors[&node].not();
                        // check to see if we've colored the neighbor already
                        if let Some(&old_color) = colors.get(&neighbor) {
                            if old_color != new_color {
                                is_bipartite = false;
                            }
                        } else {
                            colors.insert(neighbor, new_color);
                        }
                    }
                }
            }
        }

        Ok((nodes.into_iter(), is_bipartite))
    }

    pub fn bfs(&self, start: &usize) -> Result<impl Iterator<Item = usize> + '_, String> {
        match self.bfs_bipartite(start) {
            Err(err) => Err(err),
            Ok((iter, _)) => Ok(iter),
        }
    }

    pub fn is_bipartite(&self) -> bool {
        if self.order() == 0 || self.size() == 0 {
            return false;
        }
        let start = self.nodes.keys().take(1).next().unwrap();
        match self.bfs_bipartite(start) {
            Ok((_, is_bp)) => is_bp,
            _ => false,
        }
    }

    // Parallel edge-specific functions
    pub fn min_cut(&mut self) -> usize {
        let mut nodes = self.order();
        let mut rng = rand::thread_rng();
        while nodes > 2 {
            let total_edges = self.size();
            let dead_edge: usize = rng.gen_range(0..total_edges);
            let mut current_edge_count = 0;
            let mut edge_id: (usize, usize) = (0, 0);
            for edge in self.edges() {
                current_edge_count += edge.size();
                if dead_edge < current_edge_count {
                    edge_id = edge.id();
                    break;
                }
            }
            self.collapse(&edge_id);
            nodes = self.order();
        }
        self.size()
    }

    fn collapse(&mut self, edge_id: &(usize, usize)) {
        let &(winner, loser) = edge_id;
        self.edges.remove(edge_id);
        // Note: we have to collect the iterator into a vector so that we're not
        // borrowing it when we go to reparent orphan nodes in the for-loop.
        for &neighbor in self.neighbors(loser).unwrap().collect::<Vec<_>>().iter() {
            let edge_id = if self.edge_type == EdgeType::Undirected {
                sort_edge((loser, neighbor))
            } else {
                // neighbors() will only return children in a directed graph
                (loser, neighbor)
            };
            if let Some(weights) = self.edges.remove(&edge_id) {
                let edge = Edge::new(winner, neighbor, weights.weights);
                self.add_edge(edge);
            }
        }

        self.nodes.remove(&loser);
        self.neighbors.remove(&loser);
        self.parents.remove(&loser);
    }
}
impl Default for Graph {
    fn default() -> Self {
        Graph::directed()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_builds_undirected() {
        let mut g = Graph::undirected();

        let edges = (0..5)
            .into_iter()
            .collect::<Vec<usize>>()
            .windows(2)
            .map(|w| Edge::plain(w[0], w[1]))
            .collect::<Vec<_>>();

        let updated_edges = g.add_edges(&edges);

        assert_eq!(updated_edges, 0);

        g.add_edge(Edge::new(0, 4, vec![1.0]));

        assert_eq!(5, g.order());
        assert_eq!(g.edge((4, 0)).unwrap().weights, vec![1.0_f64]);
        assert_eq!(g.edge((3, 4)).unwrap().weights, vec![0.0_f64]);
        assert_eq!(g.edge((0, 4)).unwrap().weights, vec![1.0_f64]);
        assert_eq!(g.edge((1, 2)).unwrap().weights, vec![0.0_f64]);
        assert!(g.edge((0, 3)).is_err());
        assert_eq!(
            g.neighbors(4).unwrap().collect::<HashSet<usize>>(),
            [0, 3].iter().copied().collect::<HashSet<usize>>()
        );
        assert_eq!(g.neighbors(4).unwrap().count(), 2);
    }

    #[test]
    fn it_builds_directed() {
        let mut g = Graph::directed();

        let edges = (0..5)
            .into_iter()
            .collect::<Vec<usize>>()
            .windows(2)
            .map(|w| Edge::plain(w[0], w[1]))
            .collect::<Vec<_>>();

        let updated_edges = g.add_edges(&edges);

        assert_eq!(updated_edges, 0);

        g.add_edge(Edge::new(0, 4, vec![1.0]));

        assert_eq!(5, g.order());

        assert!(g.edge((4, 0)).is_err());
        assert_eq!(g.edge((0, 4)).unwrap().weights, vec![1.0]);
        assert_eq!(g.edge((1, 2)).unwrap().weights, vec![0.0]);
        assert!(g.edge((0, 3)).is_err());
    }

    #[test]
    fn it_has_degrees() {
        let mut gd = Graph::directed();

        gd.add_edge((0, 1).into());
        gd.add_edge((0, 2).into());
        gd.add_edge((0, 3).into());
        // classic diamond graph
        gd.add_edge((1, 4).into());
        gd.add_edge((2, 4).into());
        gd.add_edge((3, 4).into());

        // node 0 has three neighbors
        assert_eq!(3, gd.degree(0).unwrap());
        // node 1, 2, and 3 have 1 neighbor
        assert_eq!(1, gd.degree(1).unwrap());
        assert_eq!(1, gd.degree(2).unwrap());
        assert_eq!(1, gd.degree(3).unwrap());
        // node 4 has no neighbors
        assert_eq!(0, gd.degree(4).unwrap());

        // OK, fuck shit up with [un]direction
        let mut gu = Graph::undirected();

        gu.add_edge((0, 1).into());
        gu.add_edge((0, 2).into());
        gu.add_edge((0, 3).into());
        // classic diamond graph
        gu.add_edge((1, 4).into());
        gu.add_edge((2, 4).into());
        gu.add_edge((3, 4).into());

        // node 0 has three neighbors
        assert_eq!(3, gu.degree(0).unwrap());
        // node 1, 2, and 3 have two neighbors
        assert_eq!(2, gu.degree(1).unwrap());
        assert_eq!(2, gu.degree(2).unwrap());
        assert_eq!(2, gu.degree(3).unwrap());
        // node 4 has three neighbors
        assert_eq!(3, gu.degree(4).unwrap());
    }

    #[test]
    fn black_and_white_thinking() {
        let black = Bipartition::Black;
        let white = !black;
        assert_eq!(white, Bipartition::White);
        assert_eq!(black, !white);
    }

    #[test]
    fn undirected_bipartite() {
        let mut g1 = Graph::undirected();
        /*
        edges:
        5 2
        4 2
        3 4
        1 4
         */
        for chunk in [5, 2, 4, 2, 3, 4, 1, 4].chunks_exact(2) {
            g1.add_edge((chunk[0], chunk[1]).into());
        }
        assert!(g1.is_bipartite());

        // there's a 1,4 edge, 2,4 edge, and a 3,4 edeg; adding an 1,3 edge means that
        // it's not bipartite
        g1.add_edge((1, 3).into());
        assert!(!g1.is_bipartite());

        let mut g2 = Graph::undirected();
        /*
        1 2
        4 1
        2 3
        1 3
         */
        for chunk in [1, 2, 4, 1, 2, 3, 1, 3].chunks_exact(2) {
            g2.add_edge((chunk[0], chunk[1]).into());
        }
        assert!(!g2.is_bipartite());
    }

    #[test]
    fn finds_bfs_undirected() {
        let mut g1 = Graph::undirected();
        /*
        edges:
        5 2
        4 2
        3 4
        1 4
         */
        for chunk in [5, 2, 4, 2, 3, 4, 1, 4].chunks_exact(2) {
            g1.add_edge((chunk[0], chunk[1]).into());
        }

        let bfs = g1.bfs(&1).unwrap().collect::<Vec<_>>();
        assert_eq!(5, *bfs.last().unwrap());
        assert_eq!(1, bfs[0]);
        g1.add_edge(Edge::plain(5, 6));
        let bfs = g1.bfs(&1).unwrap().collect::<Vec<_>>();
        let idx = bfs.len() - 2;
        assert_eq!([5, 6], bfs[idx..]);
    }

    #[test]
    fn it_sorts_tuple() {
        let x = 1;
        let y = 0;
        let mut t = (x, y);

        assert_eq!(1, t.0);
        assert_eq!(0, t.1);
        t = sort_edge(t);
        assert_eq!(0, t.0);
        assert_eq!(1, t.1);
    }
}
