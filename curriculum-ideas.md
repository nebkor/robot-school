Math (* for important):
 - linear algebra *
 - 3D operations with matrices
  - quarternions?
 - probability and stats *

CS:
 - coursera algorithms *
 - coursera data structures *
 - dynamic programming textbook

Coding:
 - dendreem *
 - joesort *
  - multi-threaded joesort?
 - poompoom (recursive dependency shaker/inliner)
 - something with the realsense camera depth info

Robots:
 - https://github.com/osrf/gazebo - robot simulation package for linux
  - https://ignitionrobotics.org/docs/all/getstarted - gazebo rebranded
  - https://github.com/search?q=org%3Aignitionrobotics+label%3A%22good+first+issue%22&state=open&type=Issues
 - ROS *
 - https://www.coursera.org/specializations/modernrobotics *
