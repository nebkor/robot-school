use joeraph::*;

fn main() -> Result<(), String> {
    let mut g = Graph::undirected();

    let lines = std::fs::read_to_string("./kargerMinCut.txt").map_err(|_| "whoops".to_string())?;
    let lines = lines.split_terminator('\n');
    for line in lines {
        let mut line = line
            .split_ascii_whitespace()
            .map(|n| n.parse::<usize>().unwrap());
        let node = line.next().unwrap();
        for neighbor in line {
            let edge = (node, neighbor);
            let insert = g.edge(edge).is_err();
            if insert {
                g.add_edge(edge.into());
            }
        }
    }

    let cuts = g.min_cut();
    println!("{}", cuts);

    Ok(())
}
