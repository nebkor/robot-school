fn get_pivot<T: Copy + Ord + std::fmt::Debug>(list: &[T]) -> usize {
    let len = list.len();
    let h = len - 1;
    let mid = h / 2;

    if len < 3 {
        return 0;
    }

    let a = (list[0], list[mid], list[h]);
    let (fv, mv, lv) = a;

    // the first is between the last and middle
    if (fv < lv && mv < fv) || (fv < mv && lv < fv) {
        return 0;
    }
    // the middle is between the first and the last
    if (mv < lv && fv < mv) || (mv < fv && lv < mv) {
        return mid;
    }

    // the last is between the first and the middle
    if (lv < mv && fv < lv) || (lv < fv && mv < lv) {
        return h;
    }

    mid
}

fn qs<T: Copy + Ord + std::fmt::Debug>(list: &mut [T]) -> usize {
    if list.len() < 2 {
        return 0;
    }

    let len = list.len();
    let mut comps = len - 1;

    let mut pidx = get_pivot(&list);
    list.swap(0, pidx);
    let pivot = list[0];

    let mut min = 1;

    for i in 1..len {
        if list[i] < pivot {
            list.swap(i, min);
            min += 1;
        }
    }

    pidx = min - 1;
    list.swap(0, pidx);

    comps += qs(&mut list[0..pidx]);
    comps += qs(&mut list[(pidx + 1)..]);

    comps
}

fn main() {
    let mut ints: Vec<i32> = if true {
        std::fs::read_to_string("./qs.txt")
            .expect("oh no")
            .as_str()
            .split_ascii_whitespace()
            .map(|i| i.parse().unwrap())
            .collect()
    } else {
        (1..=4).into_iter().rev().collect()
    };

    //dbg!(&ints[0..20], &ints[9979..9999]);

    //dbg!(&ints);

    let comps = qs(&mut ints);

    println!("\ncomps: {}\n", comps);

    //dbg!(&ints);

    //dbg!(&ints[0..20], &ints[9979..9999]);
}
