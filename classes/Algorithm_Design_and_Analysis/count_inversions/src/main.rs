use std::{cmp::Ordering, fmt::Debug};

fn sort_and_count_invs<T: PartialOrd + Copy + Debug>(vals: &mut [T], order: Ordering) -> usize {
    let len = vals.len();
    if len < 2 {
        return 0;
    }

    let mid = len / 2;
    let (l_sorted, r_sorted) = vals.split_at_mut(mid);
    let l_count = sort_and_count_invs(l_sorted, order);
    let r_count = sort_and_count_invs(r_sorted, order);

    let last_l = l_sorted.len();

    let mut count = l_count + r_count;

    // now merge
    let mut lidx = 0;
    let mut ridx = 0;
    let mut sorted = Vec::with_capacity(len);

    for _ in 0..len {
        let l = l_sorted[lidx];
        let r = r_sorted[ridx];

        if let Some(ord) = r.partial_cmp(&l) {
            if ord == order {
                count += last_l - lidx;
                ridx += 1;
                sorted.push(r);
                if ridx == r_sorted.len() {
                    sorted.extend_from_slice(&l_sorted[lidx..]);
                    break;
                }
            } else {
                lidx += 1;
                sorted.push(l);
                if lidx == l_sorted.len() {
                    sorted.extend_from_slice(&r_sorted[ridx..]);
                    break;
                }
            }
        }
    }

    vals.copy_from_slice(&sorted[..]);

    count
}

fn main() {
    // let mut vals = [0, 1, 2, 3];
    let mut vals = [3, 2, 1, 0];
    //let mut vals = [1, 3, 2, 4, -5];
    // let mut vals = [2, 1, 3, 4];
    // let mut vals = [1, 2, 4, 3];

    let count = sort_and_count_invs(&mut vals, Ordering::Less);

    // let mut ints: Vec<i32> = std::fs::read_to_string("./integer_array.txt")
    //     .expect("oh no")
    //     .as_str()
    //     .split_ascii_whitespace()
    //     .map(|i| i.parse().unwrap())
    //     .collect();

    // let count = sort_and_count_invs(&mut ints, Ordering::Less);

    println!("inversions: {}", count);
}
